from django.urls import path
from projects.views import list_projects, details_project, create_project

urlpatterns = [
    path("create/", create_project, name="create_project"),
    path("<int:id>", details_project, name="show_project"),
    path("", list_projects, name="list_projects"),
]
