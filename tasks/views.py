from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import TasksForm
from django.contrib.auth.decorators import login_required


@login_required
def create_task(request):
    if request.method == "POST":
        form = TasksForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = TasksForm()
    context = {"form": form}
    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    tasks = Task.objects.filter(assignee=request.user)
    context = {"tasks": tasks}
    return render(request, "tasks/tasks.html", context)
